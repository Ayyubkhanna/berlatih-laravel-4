<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Day 2
Route::get('/home', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

//Day 3
Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/', function () {
    return view('day3.index');
});

Route::get('/data-tables', function () {
    return view('day3.datatables');
});